\chapter{L'identità digitale} % Chapter title
\label{ch:1-digital-identity} 

\section{Il concetto di identità}
Dare una definizione di identità è un compito tutt'altro che banale. Nel mondo reale, un'identità è sostanzialmente un concetto astratto che si riferisce all'insieme di attributi (come ad esempio le generalità, l'aspetto, la personalità ...) che in qualche modo caratterizza una persona fisica e che permette di \textit{riconoscerla} tra le altre, in modo da capire se rappresenta un perfetto sconosciuto, un conoscente o un caro amico che conosciamo da tempo.\\

\noindent Provare di essere una certa identità, o più semplicemente una certa persona, è in questo scenario è piuttosto semplice. Il più delle volte, quando la presenza fisica stessa non è abbastanza, è sufficiente mostrare un documento (solitamente fornitoci da una certa \textit{autorità}) per far si che verificare che siamo chi diciamo di essere, risulti per il nostro interlocutore un lavoro triviale. Una volta riconosciuti si può beneficiare della fiducia ed affidabilità che è stata eventualmente costruita nel tempo e che rimane in qualche modo connessa all'identità stessa.

\section{Dall'identità all'identità digitale}\label{sec:identity-to-digital-identity}

\noindent Con la pervasività di internet e di un mondo interamente connesso, è divenuto un aspetto chiave poter applicare i concetti relativi all'identità descritti nella sezione precedente anche in questo nuova rivoluzione digitale. L'obiettivo ideale, è quello di poter interagire con altre identità (digitali), completando transazioni in totale fiducia, così come se tutto ciò avvenisse nel mondo fisico.\\

\noindent Nella realtà dei fatti, permettere tutto ciò è un compito arduo che porta con se diversi punti aperti ed alcune problematiche. Di seguito sono descritti più nel dettaglio due concetti chiave che ruotano attorno all'immagine di identità digitale, l'\textbf{autenticazione} e l'\textbf{autorizzazione}.

\subsection{Autenticazione}\label{sub:authentication}
L'autenticazione è l'atto con la quale un identità prova se stessa. Quando si tratta di persone fisiche, esse devono sostanzialmente utilizzare un protocollo per dimostrare che, quella che sta interagendo, è la propria identità digitale. Ma non solo: all'interno dell'ambiente digitale fanno capo anche entità e servizi automatici che non rappresentano alcuna persona fisica, ma che interagiscono con la rete circostante come le fossero, ed è solitamente necessario che anch'essi completino il processo di autenticazione.\\

\noindent E' possibile semplificare ulteriormente la definizione data fin'ora, definendo l'autenticazione come l'insieme dei processi che permettere di rispondere alla domanda:
\begin{displayquote}
"Sei davvero chi dici di essere?"
\end{displayquote}

\noindent Talvolta però, anche ciò non è sufficiente. C'è infatti un altro aspetto delicato relativo all'autenticazione che va analizzato con cura. Per definirlo, la domanda che è necessario porsi cambia solo leggermente, ma assume un significato piuttosto diverso:

\begin{displayquote}
"Sei \textit{ancora} chi dici di essere?"
\end{displayquote}

\noindent Si può prendere in esempio \texttt{Alice}, che all'iscrizione verso un \textit{mail provider} invia i propri documenti (e assumendo che il mail provider sia fidato), dà garanzia che le mail dirette alla sua casella postale siano effettivamente diretta a lei come persona fisica. Nonostante ciò, se le credenziali che \texttt{Alice} utilizza successivamente per effettuare il login alla casella postale, non sono protette adeguatamente, non è più possibile sapere con certezza se la persona con cui stiamo scambiando le email sia ancora la stessa.\\

\noindent Ad oggi, quello dell'autenticazione è un tema in continua evoluzione e i processi (o protocolli) si dividono principalmente in tre categorie. In particolare, quando un'entità vuole autenticarsi può fornire come \textit{prova}:

\begin{enumerate}
    \item \label{item:auth-know}Ciò che conosce (una \textit{password} o un segreto) ;
    \item \label{item:auth-have}Ciò che possiede (un \textit{token} o un certificato) ;
    \item \label{item:auth-is}Ciò che è (impronte digitali, biometria...) . 
\end{enumerate}

\noindent Ciascuna voce dell'elenco, possiede punti di forza e debolezze (possibilità di smarrimento per \ref{item:auth-know} e \ref{item:auth-have}, falsi positivi o clonazione per \ref{item:auth-is}) e per garantire uno schema più robusto, ciò che viene utilizzato oggi è una combinazione di essi, che prendere il nome di \ac{2FA} nel caso di utilizzo di due meccanismi o più genericamente \ac{MFA} quando il loro numero è variabile.

\subsection{Autorizzazione}
Definita l'autenticazione, ed assumendo che essa sia implementata correttamente all'interno di un sistema, le possibili problematiche sono tutt'altro che terminate. Il secondo aspetto da gestire, di altrettanta importanza, è quello di poter garantire che ciascuna identità all'interno del sistema abbia tutti \textbf{e soli} i permessi necessari a far ciò che è adibito (o appunto autorizzato) a fare. Tornando ad \texttt{Alice}, una gestione sbagliata delle autorizzazioni potrebbe ad esempio permetterle di accedere alle caselle di posta dei suoi colleghi (o ancor peggio dei suoi superiori), o di cambiare loro la password.\\

\noindent Data la sua importanza, questo argomento verrà analizzato con maggiore dettaglio nei capitoli successivi, nella quale viene evidenziato come la complessità della sua gestione su larga scala renda necessaria un'apposita infrastruttura all'interno di un azienda.\\

\section{L'importanza dell'identità digitale}
Introducendo l'autenticazione è emersa la difficoltà di provare di essere ciò che si dice di essere nel mondo digitale, ma il problema ha conseguenze molto più gravi quando non è possibile nemmeno provare la propria identità fisica. Un'analisi in dettaglio sull'argomento svolta da \cite{digital-identity-benefits} evidenzia come più di mezzo miliardo di persone al mondo non dispongano di un documento di identità (nemmeno cartaceo), che impedisce loro di partecipare attivamente alla vita quotidiana dal punto di vista sociale, politico, culturale ed economico. In questo scenario, la comparsa di un sistema che garantisca l'identità digitale sarebbe un enorme passo avanti non solo da un punto di vista innovativo, ma anche un notevole guadagno economico (lo stesso documento stima circa un risparmio dei contribuenti di 50 miliardi di dollari all'anno) ed un'opportunità per le organizzazioni pubbliche di servire in modo mirato le persone che più ne necessitano.\\

Tra i principali benefici che l'identità digitale può portare in questo scenario, lo stesso documento evidenzia :

\begin{enumerate}
    \item Inclusione dal punto di vista economico: basti pensare che chi non ha la possibilità di provare la propria identità non può aprire un conto bancario o in diversi paesi nemmeno acquistare una SIM per il telefono;
    
    \item Accesso alla sanità, l'identità digitale permettere di identificare facilmente persone in difficoltà per far sì che possano beneficiare di \textit{benefits} e servizi particolari, oltre che tracciare con maggiore cura le zone nella quale sono necessari vaccini e trattamenti per malattie infettive come l'HIV;
    
    \item Parità di genere: in diverse aree del mondo per le donne è più difficile ottenere i documenti, impedendo loro di far valere i loro diritti e di oltrepassare le barriere sociali;
    
    \item \textit{Governance}: l'identità digitale ha la potenzialità di aumentare significativamente l'efficienza nonché la trasparenza dei servizi governativi; la transazioni online sono meno costose, con l'ulteriore vantaggio di portare con sè un terreno difficile per furti e corruzioni.tre
    
\end{enumerate}{}

\section{Verso l'online identity}
Definita cos'è e quali sono gli aspetti chiave dell'identità digitale presa singolarmente, il passo successivo è quello di estenderne via via i confini per comprendere quali sono i principali punti aperti a cui si va incontro ipotizzando la creazione di un'infrastruttura globale , come quella descritta poco fa, grazie alla quale ogni persona fisica che utilizza un servizio online, interagisca con esso tramite la propria identità digitale. In questo scenario, come sottolineato da \cite{digital-identity}, emergono principalmente quattro \textit{sfide} a cui far fronte:

\begin{description}
    \item [Privacy]: La protezione della privacy degli utenti è un aspetto di estrema importanza. Una transazione online nella quale un entità acquista un prodotto da un negozio, costringe la prima a fornire le proprie informazioni ed a renderla vulnerabile da questo punto di vista. Lo scenario peggiora quando per un errore o per malfunzionamento, i dati ottenuti finiscono nelle mani sbagliate.
    \item [Sicurezza]: Qualsiasi transazione online, ed in particolari quelle che coinvolgono \textit{asset} di grande valore, devono avvenire in sicurezza. Garantire che un sistema  è \textit{trusted} (ossia che si può riporre fiducia) è certamente un aspetto chiave.
    \item [Furto di identità]: Esattamente come spiegato nella sezione \ref{sec:identity-to-digital-identity} è necessario riuscire a garantire che ciascun entità sia esattamente chi dice di essere, o eventualmente aver costruito procedure da mettere tempestivamente in atto quando ciò non avviene. E' necessario evidenziare inoltre, che come tipologia di attacco quello del furto di identità trova terreno fertile nel \textit{cyberspazio} in maniera notevolmente maggiore rispetto al mondo fisico.
    \item [Interoperabilità]: E' necessario infine garantire che i diversi servizi collaborino tra loro, ossia che un identità fornita da una certa organizzazione sia riconosciuta da un'altra e viceversa. A questo proposito è utile aggiungere che \ac{SAML} potrebbe essere un possibile standard (già attualmente utilizzato) per far fronte al problema.
\end{description}


\noindent Nonostante i punti aperti, su scala ridotta sono già comparsi sistemi analoghi a quello appena descritto, come ad esempio il \ac{SPID}: all'interno di \ac{SPID} (ampiamente descritto in \cite{spid}) i cittadini italiani hanno la possibilità di ottenere un'unica identità digitale con la quale è possibile accedere a diversi servizi messi a disposizione dai cosiddetti \textit{service providers}, che corrispondono in questo caso ai servizi online di pubblica amministrazione.
