
\chapter{Identity IQ} % Chapter title
Durante la parte iniziale dello stage, come qualsiasi nuova attività, è stato fondamentale studiare ed apprendere le tecnologie utilizzate. Tra queste, quella di maggiore importanza è stata la piattaforma attorno alla quale ruota l'intero progetto di \textit{identity governance}: \textbf{Sailpoint IdentityIQ}. A seguire questo paragrafo, verrà fatta dapprima un'introduzione qualitativa della piattaforma, analizzandone limitatamente pro e contro a partire dalle valutazioni fatte da \href{https://www.gartner.com/en}{Gartner}, per poi continuare descrivendo ad alto livello la piattaforma e di come è stata configurata all'interno dell'azienda, focalizzandosi maggiormente sulla parti necessarie a comprendere le procedure tecniche di cui si parlerà a partire dal capitolo \ref{ch:4-implementation-introduction}.

\section{Gartner ed il Magic Quadrant}
Gartner Inc è una multinazionale leader nel settore delle ricerche di mercato che pubblica ogni due anni circa diversi report contenenti analisi qualitative inerenti a  diversi settori di mercato, che spaziano dalla finanza fino all'\ac{IT}. Tra le altre, è presente un'analisi di mercato (\cite{magic-quadrant}) relativa all'\ac{IAG} che, a partire dai punti seguenti:

\begin{enumerate}
\item Ciclo di vita delle identità digitali
\item Gestione degli Entitlement
\item Richieste di accesso
\item Workflow
\item Policy e gestione dei ruoli
\item Certifica
\item Fulfillment
\item Auditing
\item Report ed analisi di dati sulle identità digitali
\end{enumerate}

\noindent valuta e confronta i diversi \textit{vendors} protagonisti di questo scenario, ponendoli all'interno di un diagramma, riportato nella figura \ref{fig:magic-quadrant}, chiamato \textit{magic quadrant}. \\

\begin{figure}[t!]
	\centering
	{\includegraphics[width=\hsize]{gfx/magic-quadrant.png}}
	\caption[Il Magic Quadrant di Gartner]{Magic quadrant relativo all'\ac{IAG}}\label{fig:magic-quadrant}
\end{figure} 


\noindent La realizzazione del diagramma è svolta partendo da un punteggio calcolato dalla combinazione della \textit{Completeness of vision} (completezza di visione) ed \textit{ability to execute} (capacità di mettere in pratica) con la quale ogni \textit{vendor} guadagna un posto in uno dei quattro quadranti. Ciascun quadrante ha un significato specifico :

\begin{description}
\item[Leader] Rientrano questo quadrante i \textit{vendors} leader (appunto) nel settore, che operano nelle cinque principali regioni geografiche e che detengono una sicurezza finanziaria consolidata. Essi detengono inoltre una consistente fetta del mercato, sono in grado di capirne le esigenze e di guidare verso di esso eventuali innovazioni.
\item[Sfidanti] All'interno di questo quadrante sono presente i \textit{vendors} che potrebbero rappresentare una minaccia futura per gli \textit{leaders}, essi fan parte del mercato in misura certamente non trascurabile, ma non dispongono ancora di sufficiente influenza.
\item[Visionari] Il quadrante dei visionari è composto da coloro che hanno introdotto prodotti innovativi, ma che non possiedono una fetta di mercato significativa da cui tratte un solido profitto. I \textit{vendors} in questione sono solitamente aziende private che vengono inglobate da aziende molto più grandi.
\item[Giocatori di nicchia] Come suggerisce il nome, all'interno di questo quadrante sono presenti i \textit{vendors} a capo di prodotti di nicchia, molto specifici e certamente innovativi su un determinato aspetto ma che impediscono loro di entrare nel mercato vero e proprio o di essere assorbiti da aziende più grandi.
\end{description}


\noindent Come è osservabile dalla figura \ref{fig:magic-quadrant} sopra, \textit{IdentityIQ} è leader nel settore \ac{IAG}, con un significativo stacco dai rivali. Uno dei principali punti di forza di questa soluzione è certamente il numero di \textit{connettori}\footnote{Un connettore è un driver che mette in comunicazione la piattaforma con un'applicazione. Un esempio sono i connettori \ac{JDBC} o \ac{LDAP}} che essa offre (100+ compresi nel prodotto, oltre che quelli acquistabili a parte), che la rende estremamente flessibile dal punto di vista delle integrazioni con le applicazioni. Non solo, \textit{IdentityIQ} comprende un motore di \textit{analytics} particolarmente efficiente che permette di suggerire i ruoli per ciascuna identità a seconda delle abilitazioni che possiede oltre che un tool di \textit{Unstructured Data Access Governance} con la quale + possibile fare \textit{governance} oltre che delle identità, anche dei file presenti all'interno dei sistemi gestiti. Il rovescio della medaglia per questa piattaforma, risiede nella sua difficoltà di personalizzazione rispetto alle esigenze di clienti, che spesso sono costretti ad adattare il proprio modello dati e non viceversa, che risulta tra l'altro particolarmente inefficiente nel caso in cui il numero di identità e di ruoli cresce significativamente.

\section{Overview della piattaforma}
\textit{IdentityIQ} è un software estremamente esteso e ricco di funzionalità e la sua descrizione completa esula dallo scopo di questo documento. Per descrivere il suo funzionamento, è necessario fare un piccolo passo indietro e ritornare a parlare di identità digitale, il cui ciclo di vita all'interno della piattaforma è mostrato nella figura \ref{fig:identity-lifecycle} e spiegato nella sezione \ref{sub:identity-lifecycle}. Successivamente nella sezione \ref{sub:identityiq-provisioning} si parlerà delle applicazioni che ruotano attorno alla piattaforma nel contesto azienda, e verrà data una descrizione più globale dell'intera architettura.

\begin{figure}[t!]
	\centering
	{\includegraphics[width=\linewidth]{gfx/identity-lifecycle.png}}
	\caption{Ciclo di vita di un identità digitale all'interno di \textit{IndetityIQ}}\label{fig:identity-lifecycle}
\end{figure} 

\subsection{Ciclo di vita dell'identità digitale}\label{sub:identity-lifecycle}
\subsubsection{Joiner} \label{subsub:joiner}
Quello di \textit{joiner} è un processo che viene fatto scattare ad una nuova assunzione all'interno dell'azienda. Il flusso nasce al momento dell'assunzione di un dipendente (o di un \textit{contractor}) che implica l'inserimento dei suoi dati all'interno di una \textbf{sorgente autoritativa}\footnote{Una sorgente autoritativa può essere un'applicazione, o più semplicemente un database o un servizio di directory, dalla quale sono prelevati i dati delle identità e considerati come attendibili}, che in questo caso corrisponde ad un servizio di directory dell' \ac{HR}. A partire da ciò, viene create la sua identità digitale alla quale vengono immediatamente creati e legati gli accessi ai principali servizi (internet, intranet, account di posta elettronica ...) oltre che alle applicazioni ad egli necessarie (che variano a seconda del "dipartimento" specifico all'interno della quale lavorerà). Inoltre per \textit{policy} aziendale ogni identità è sottoposta ad un'altra identità che agisce come suo responsabile, permettendo in questo modo la costruzione ed il mantenimento di una struttura complessivamente gerarchica.

\subsubsection{Mover} \label{subsub:mover}
Come già menzionato in precedenza, all'interno di una grande azienda è comune che i dipendenti si muovano all'interno di diversi dipartimenti o sotto-società dell'azienda stessa, ed il processo che prevede questo tipo di cambiamento prende il nome di \textit{mover}. A differenza di \textit{joiner} e \textit{leaver} nella quale l'identità digitale (chiamata \textbf{Identity Cube} all'interno della piattaforma) viene rispettivamente creata e distrutta, in questo caso non subisce direttamente modifiche. Ciò che avviene infatti, è solamente un cambio di permessi (revoche sul vecchio sistema e richieste di accessi sul nuovo) legati all'identità, che rimane la stessa su entrambi i sistemi.

\subsubsection{Leaver} \label{subsub:leaver}
Il processo di \textit{leaver}, che corrisponde (ed è ciò che è causato) più semplicemente alla cessazione di un contratto di lavoro, si occupa della revoca degli accessi legati alla relativa identità digitale ed alla cancellazione dell'identità stessa. Il flusso di questo processo non è triviale e comprende numerose varianti, date dal fatto che l'utente cessato può essere responsabile di altri dipendenti o (unico) responsabile di accettare/revocare gli accessi ad una determinata applicazione\footnote{Gli utenti con tali responsabilità vengono chiamati \textit{client manager}}, motivo per cui le identità responsabili dell'utente cessato, vengono notificate più volte nelle settimane precedenti alla cessazione stessa.

 
 \subsection{Le applicazioni}\label{sub:identityiq-provisioning}
 Definito il ciclo di vità di un'identità digitale all'interno di \textit{IdentityIQ}, è possibile spostarsi verso ciò per cui l'infrastruttura stessa viene costruita: le applicazioni. Il termine, in questo scenario, può risultare un po' ambiguo in quanto le applicazioni assumono diverse forme (dai \textit{web-services} ad applicazioni \textit{stand-alone}, \ac{SAP} ...) ed in generale servizi alla quale accedono ed utilizzano le persone che lavorano con e per l'azienda. Per il lavoro svolto, una distinzione chiave da fare sin da subito e che verrà approfondita nel capito successivo, è quella di dividere le applicazioni in principalmente in due insiemi:
 
 \begin{description}
\item[Applicazioni connesse] Un'applicazione è connessa se è direttamente connessa ad \textit{IdentityIQ}, che in ogni momento ed in tempo reale può \textit{aggregare} nuovi dati dall'applicazione, o effettuare il \textit{provisioning} verso di essa. Per permettere ciò, è necessario un \textbf{connettore} che metta in comunicazione le due parti, di cui una grande varietà è offerta dalla piattaforma stessa. Un esempio di questo insieme è formato dalle applicazioni connesse tramite \ac{JDBC} e \ac{LDAP}.
\item[Applicazioni non connesse] Quando \textit{IdentityIQ} possiede le informazioni relative agli account ed alle abilitazioni su una applicazione, ma non può leggere e scrivere in tempo reale su di essa, l'applicazione è definta non connessa. In questo caso, ciascuna modifica è mediata da un operatore fisico, che nel contesto aziendale prende il nome di \textbf{application manager} o più semplicemente AM. 
 \end{description}
 
\subsection{Gli "orfani" e le identità "correlate"}\label{sub:orphan-correlated}
 
 
\noindent Definito ciò, è necessario fare nuovamente un passo indietro e tornare sulle identità, che sono ora scindibili anch'esse principalmente in due insiemi: \textbf{correlate} ed \textbf{orfane}. Come è stato descritto nel processo di \textit{mover}, le applicazioni con la quale un'identità interagisce può variare nel tempo, e nuovi account possono essere creati sulle nuove applicazioni. Alla creazione di un nuovo \textit{account}, \textit{IdentityIQ} cercherà di capire\footnote{Le regole con la quale questa correlazione avviene possono essere completamente personalizzate} se l'account creato (che all'interno della piattaforma prende il nome di \textbf{Link}) corrisponde ad un identità digitale già presente all'interno della piattaforma, da cui si distinguono due casi:

\begin{itemize}
    \item L'identità digitale esiste e proviene da una \textbf{sorgente autoritativa}, \textit{IdentityIQ} aggiunge l'\textit{account} all'identità ed essa è definita \textbf{correlata}. Questo tipo di identità sono maggiormente fidate, in quanto provenendo da una sorgente autoritativa si sa chiaramente chi sono.
    \item Non esiste un identità digitale, oppure esiste ma non proviene da una sorgente autoritativa (ad esempio proviene da un'altra applicazione qualsiasi), in questo caso l'identità viene definita \textbf{orfana}.
\end{itemize}

\noindent In un'infrastruttura ideale, tutte le applicazioni sono connesse e non sono presenti orfani, ma arrivare a questo stato in un sistema molto complesso non è un compito banale.

\subsection{L'infrastruttura globale}

\begin{figure}[h!]
	\centering
	{\includegraphics[width=\hsize]{gfx/architecture.png}}
	\caption[L'Architettura di un sistema di \ac{IAG}] {Esempio di architettura di un sistema di \ac{IAG}}\label{fig:identityiq-arch}
\end{figure} 

\noindent A questo punto è possibile analizzare un quadro più ampio osservando nell'immagine \ref{fig:identityiq-arch} un esempio di come potrebbe essere un'intera infrastruttura di \ac{IAG} utilizzando \textit{IdentityIQ}.\\

\noindent Si parte da un insieme di applicazioni a cui di norma l'utente-lavoratore, può direttamente accedere.Il primo passo consiste nel porre nel mezzo un sistema di \textit{Access Management} (i cui principali compiti sono stati spiegati nella sezione \ref{sec:identity-to-digital-identity}) che integra un servizio di \ac{SSO} ed eventualmente un sistema di gestione degli accessi privilegiati (ad esempio \href{https://www.cyberark.com/}{CyberArk}), in maniera tale da gestire in modo centralizzato tutti gli accessi alle diverse applicazioni. Spostandosi sulla parte di \textit{Governance}, qui avviene l'intera gestione delle utenze, delle responsabilità e delle abilitazioni; il ruolo fondamentale è quello comunicare con le applicazioni (tramite \textit{riconciliazione} e \textit{provisioning}) oltre che leggere e scrivere sui \textit{servizi di directory} utilizzati dall'\textit{Access Management}. Inoltre, fa sempre parte della \textit{governance} la configurazione nonchè la verifica, in seguito a qualsiasi evento, che le policy aziendali rimangano sempre rispettate.\\

\noindent Dal punto di vista degli utenti, essi hanno la possibilità di accedere direttamente alla piattaforma di \textit{governance} per richiedere abilitazioni o semplicemente controllare lo stato della propria identità digitale (nella quale può eventualmente essere integrato un riepilogo della propria posizione lavorativa). Il discorso è leggermente variabile per gli utenti che sono responsabili (di altri utenti o di gestire applicazioni), i quali hanno il dovere di accedere alla piattaforma per completare le certifiche\footnote{La certifica è il processo nella quale ogni $n$ mesi i responsabili di ciascuna applicazione devono verificare che i ruoli e le abilitazioni su di esse siano conformi a quanto atteso} o gestire i propri sottoposti.\\


\noindent Al di fuori del "comportamento base" appena descritto all'interno del sistema è comune:
\begin{enumerate}
\item Utilizzare la funzionalità di \textit{reporting} grazie alla quale è possibile estrarre report standard o completamente \textit{custom} a seconda delle esigenze aziendali;
\item Scrivere dei \textit{workflow custom} con la quale sono intraprese alcune azioni in seguito ad un evento, come ad esempio l'invio di una mail in seguito al cambio di un responsabile per un dato utente, piuttosto che un \textit{workflow} che implementi l'intero processo di \textit{leaver} con azioni differenti da quelle standard;
\item Integrare sistemi di \ac{SIEM} o utilizzare le funzionalità di \textit{Auditing} presenti all'interno della piattaforma per far fronte a particolari incidenti di sicurezza.

\end{enumerate}


 
