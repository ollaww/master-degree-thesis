% Chapter 2

\chapter{Governance delle identità digitali} % Chapter title


\section{L'identità digitale in azienda} \label{sec:digital-identity-company}

\textit{Digital transformation} ed \textit{Industry 4.0} sono i motori che spingono sempre più contesti aziendali a tuffarsi nel mondo digitale, trasformando i propri servizi ed adottando nuove tecnologie che operano in modo più \textit{smart} ed efficiente. Tuttavia, come è stato evidenziato nel capitolo precedente, l'altra faccia della medaglia di questo approccio implica la necessità di prevenire ed affrontare le minacce di così detti \textit{cyber-criminali} . All'interno di una grande azienda, migliaia di dipendenti (o esterni) accedono a centinaia di servizi, ed è sufficiente che le credenziali di uno solo di essi (in particolare di \textit{amministratori}) siano compromesse ed un potenziale attaccante esterno può avere accesso ad alcune o a tutte le risorse aziendali. A tal proposito uno studio (\cite{mc-afee}) evidenzia come:\\


{\setlength{\extrarowheight}{1em}
\begin{tabularx}{\linewidth}{@{}>{\bfseries}l@{\hspace{.5em}}X@{}}
{\huge 43\% } & dei casi di \textit{data loss} (intenzionale o no) proviene da utenti interni all'azienda. \\

{\huge 62\% } & degli utenti ha accesso a dati e risorse che non dovrebbero vedere \\

{\huge 64\% } & degli esperti di sicurezza sostiene in seguito ad incidenti, che un sistema di \ac{DLP} avrebbe potuto prevenirlo \\

\end{tabularx}
}
\vspace{1em}

\noindent Oltre al "semplice" furto di credenziali, di seguito sono riportati alcuni esempi che sottolineano come nel processo di \textit{digital transformation} siano necessari diversi accorgimenti per essere realmente efficienti e mantenere basso il rischio.

\subsection{Toxic combination of rights}\label{sub:toxic-combination}
\noindent \texttt{Bob}, dipendente della grande azienda \texttt{Costruzioni}, incontra \texttt{Alice} dell'azienda \texttt{Acciaierie Alice}. Quest'ultima promette a \texttt{Bob} un ricco compenso se l'azienda \texttt{Costruzioni} farà un ordine verso la sua. Ciò è quanto succede:
\begin{enumerate}
    \item \texttt{Bob}, è uno dei dipendenti abilitati a richiedere ordine, quindi inoltra ad un'ipotetica "commissione richieste" della sua azienda un ordine di acquisto di cento mila dollari di materiale all'azienda \texttt{Acciaierie Alice}.
    \item Nella "commissione richieste", dove la richiesta giunge, solo poche persone hanno l'abilitazione a confermare o rifiutare gli ordini che giungono.
    \item \texttt{Bob}, possiede i permessi della "commissione richieste" e quando il suo ordine giunge, procede prontamente a confermarlo, riscuotendo il compenso di \texttt{Alice}.
\end{enumerate}

\noindent Ciò che è accaduto è che per errore nell'assegnazione delle abilitazioni, \texttt{Bob} ha la capacità di richiedere e di confermare gli ordini, una combinazione potenzialmente pericolosa per qualsiasi dipendente all'interno di un'azienda, che prende appunto il nome di \textit{toxic combination of right} (combinazione pericolosa di permessi), contrario al principio utilizzato per impedirlo chiamato \ac{SoD}. Sebbene questo esempio sia portato al limite, in scenari particolarmente grandi, può risultare non banale verificare (a meno di un sistema di \textit{governance}) se esiste un utente con una combinazione tale. Come verrà spiegato successivamente, si può fare fronte a questo problema mettendo in attimo la verifica delle \textbf{policy} automaticamente all'assegnazione di nuove abilitazioni.

\subsection{Mancata revoca degli accessi}\label{sub:revoke-access}

\texttt{Charlie}, importante membro dell'azienda \texttt{Software London} in seguito ad una brusca litigata con i suoi superiori, viene immediatamente licenziato. Con il suo licenziamento, è necessario far sì che a Charlie vengan rimossi gli accessi a ciascuno degli applicativi aziendali, oltre che alla mail ed alla rete aziendale. Viene quindi spedita una mail a tutti i responsabili con l'obiettivo di rimuovere gli accessi di \texttt{Charlie}. Per negligenza o dimenticanza, alcuni degli accessi non vengono revocati e quando una settimana dopo \texttt{Charlie} riesce ad accedere ad un applicativo, decide di cancellare alcuni \textit{file} importanti.\\

\noindent Si tratta nuovamente di un esempio portato al limite, ma in uno scenario leggermente diverso in cui ad esempio \texttt{Charlie} è un \textit{contractor} che lavora solo qualche mese all'interno dell'azienda ed un anno dopo ha ancora la possibilità di accedere, il pericolo causato da una mancata revoca diventa concreto, ancora più aggravato dal fatto che nel lasso di tempo in cui le credenziali rimangono valide, possono essere anche rubate.  


\subsection{Cambio temporaneo di dipartimento}

Il signor Chuck, comune dipendente della \texttt{Networking} è particolarmente abile in diversi ambiti del \textit{networking}. Capita sovente quindi, che i suoi responsabili lo mandino in alcuni dipartimenti dell'azienda a svolgere differenti lavori. Tuttavia, ogni volta che \texttt{Chuck} va in "prestito" in un dipartimento, è necessario che gli venga creata una nuova identità e che ad essa vengano assegnati i permessi e le abilitazioni corrette, e che tutto ciò venga rimosso quando \texttt{Chuck} finisce il prestito (per evitare quanto appena detto in \ref{sub:revoke-access}). E' fondamentale quindi prevedere un processo \textbf{efficiente} di assegnare e rimuovere gli accessi agli utenti per permettere che abbian la possibilità di lavorare dal primo minuto in azienda. 

\section{Identity Governance}
E' giunto il momento di introdurre il concetto di \textit{governance}, e più in particolare di \textit{\textbf{identity governance}}, che può essere definita come l'insieme dei processi e delle tecnologie che permettono di garantire che ciascun utente abbia i giusti accessi ed allo stesso tempo permetta alle aziende di visualizzare ed avere il controllo su \textbf{chi ha accesso a cosa} e se ciò è conforme alla propria \textit{policy} aziendale. Un sistema di \textit{identity governance} deve includere almeno i seguenti tre processi :

\begin{description}
\item[Policy:] determinare quali permessi possono avere le persone, in relazione a quale ruolo ricoprono all'interno dell'azienda ed a quali permessi possiedono già (come nell'esempio \ref{sub:toxic-combination})
\item[Provisioning degli account utenti:] permettere la richiesta di accesso a diversi sistemi, ed attuare automaticamente la revoca quando l'utente lascia l'azienda
\item[Certificazioni:] creazione periodica di \textit{report} (o documenti) contenenti le informazioni circa i diversi permessi che hanno le identità all'interno dell'azienda
\end{description}

\noindent Per completare il quadro è formare un sistema di \ac{IAG}, a questi processi si aggiungo quelli relativi all'\ac{IAM} (che è sostanzialmente termine che rappresenta un sottoinsieme dei processi di un sistema di \ac{IAG}), che comprendono autenticazione (\ref{sub:authentication}), \ac{SSO} o reset delle password.


\section{Vantaggi di un sistema di Governance}
Grazie ad un sistema di \textit{identity governance} è già stato menzionato come la sicurezza complessiva del sistema ne risenta in positivo, riducendo i rischi legati ai punti deboli del sistema quali \textit{passwords}, identità orfane\footnote{il concetto di identità orfana verrà approfondito successivamente nella sezione \ref{sub:orphan-correlated} quando si parlerà della piattaforma \textit{IdentityIQ}} o mancanza del principio del \ac{SoD}. Ci sono altri due punti su cui è utile evidenziare.\\

\begin{figure}[t]
	\centering
	{\includegraphics[width=\hsize]{gfx/user-centric.png}}
	\caption[\textit{User-centric} \ac{IAG}] {Rappresentazione di un sistema di \ac{IAG} \textit{user-centric}}\label{fig:user-centric}
\end{figure} 

\noindent Il primo riguarda punto riguarda il cambio di approccio nella costruzione dell'intero sistema. Molte aziende adottano infatti un approccio alla \textit{security} di tipo \textit{network-centric} (o \textit{application-centric}) nella quale l'obiettivo è quello di proteggere ciascun segmento di rete (o ciascuna applicazione) in maniera più o meno a sè state, con \textit{firewall} o \ac{IDS}, ma quest'approccio non permette di identificare le identità che hanno correttamente l'accesso senza che dovrebbero averlo, le identità ad alto rischio o se il principio del \ac{SoD} viene rispettato. L'approccio che invece offre un sistema di \ac{IAG} \textit{user-centric}, una cui rappresentazione è riportata nella figura \ref{fig:user-centric}, è quello appunto di concentrare l'attenzione sull'identità, ponendosi domande come "\textit{Chi ha l'accesso ?}", "\textit{Chi dovrebbe averlo ?}". Utilizzando quest'approccio è possibile monitorare con più attenzione i profili ad alto rischio, verificare che il principio del \ac{SoD} sia rispettato ed è più facile identificare quali identità sono collegate ad un attacco; inoltre questo tipo di approccio non esclude, ma integra, le soluzioni come \textit{firewall} o \ac{IDS} presenti nell'approccio \textit{network-based}. \\

\noindent Il secondo punto chiave, fin'ora non è stato menzionato, riguarda le regolamentazioni come la \ac{GDPR}, le quali richiedono documentazioni certificanti che all'interno dell'azienda solo le persone necessarie accedono ad informazioni sensibili. La aziende non solo devono adeguarsi alle \textit{policy} richieste, ma è necessario che esse provino che esse siano effettivamente attive e funzionanti. L'intero processo svolto senza un sistema di \textit{identity governace} può risultare estremamente lungo ed impreciso, oltre che notevolemente costoso, mentre attraverso le \textbf{certifiche} (di cui si parlerà successivamente) il processo è quasi automaticamente automatizzato.\\

\noindent Nell'ambito di questo stage l'obiettivo di integrare circa 500 nuove applicazioni attorno al sistema di \ac{IAG} è stato particolarmente stimolante dal punto di vista delle tempistiche: buona parte delle applicazioni erano (e sono) soggette alla normativa \ac{SOX} ed è stato fondamentale trovare una soluzione per far sì che fossero integrate all'interno del sistema entro la data della \textbf{certifica} perché fossero conformi alla normativa. L'intero processo sarà ripreso e spiegato più ampiamente nel capitolo \ref{ch:4-implementation-introduction}.